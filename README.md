GitHub Issues
===============

INTRODUCTION
------------

The GitHub Issues module provides a GitHub core API layer
for managing git issues from your Drupal website.

REQUIREMENTS
------------

This module requires the following modules:

 * Git Issues (https://drupal.org/project/git_issues)


INSTALLATION
------------

Install as usual.

Place the entirety of this directory in the /modules folder of your Drupal
installation. Navigate to Administer > Extend. Check the 'Enabled' box next
to the 'GitHub Issues' and then click
the 'Save Configuration' button at the bottom.

CONFIGURATION
-------------

Populate module settings form with:
GitHub API Base URL (https://api.github.com)
GitHub Access Token (https://help.github.com/articles/creating-a-personal-access-token-for-the-command-line)

## Features
* List opened and closed issues in your Drupal site
* Create, edit and close issues from your Drupal site

## Architecture
This module is plugin for other Git Issues module
